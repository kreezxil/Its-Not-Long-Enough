[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## Description

This mod replaces Minecraft's 20 minute day/night cycle to a full 24 hours!

## Note

Setting Minecraft's time to be exactly the same as real time does cause some lag,
this has a 2 minute pause period of waiting before updating the time (eventually this will be modifiable via the config)

If you wish to uninstall the mod, make sure you set the gamerule 'doDaylightCycle' to true..

## Original Mod

[Time Overhaul](https://minecraft.curseforge.com/projects/time-overhaul) - MIT LICENSED

## More Notes

I reversed compiled and reverse mapped the source from the non-obfuscated jar. There is now a github repository for the code base. Feel free to help this unique mod to go forward into the future by either forking it and rolling your own or submitting Pull Requests.

\- Kreezxil

## Modpacks

No need to ask just add. A link back to this page will be cool but not required.

## MCBBS

No need to ask, you can add it to your site as long all the download links point back here and you give me a copy of the link.

无需询问，您可以将其添加到您的网站，只要所有下载链接指向此处并且您给我一份链接。

## Help a Veteran today

I am Veteran of United States Army. I am not disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [![](http://www.kreezcraft.com/wp-content/uploads/2018/01/patreon.png)](https://patreon.com/kreezxil).

## Logo Source

[Pixabay](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpixabay.com%252fen%252ftime-clock-fractal-art-digital-2919092%252f) licensed [CC0 Creative Commons](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fcreativecommons.org%252fpublicdomain%252fzero%252f1.0%252fdeed.en)
